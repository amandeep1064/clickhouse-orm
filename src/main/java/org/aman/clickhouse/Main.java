package org.aman.clickhouse;

import org.aman.clickhouse.annotations.ClickhouseColumn;
import org.aman.clickhouse.annotations.ClickhouseTable;
import org.aman.clickhouse.annotations.OrderingKeyColumns;
import org.aman.clickhouse.annotations.models.KeyAndType;
import org.aman.clickhouse.jdbc.SampleModel;
import org.reflections.Reflections;

import java.beans.Transient;
import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {


    private static final String ARRAY_TYPE = "Array(%s)";
    private static final String SQL_COLUMN_AND_TYPE = "%s %s";
    private static final String CREATE_TABLE_PREFIX = "CREATE TABLE IF NOT EXISTS %s ( \n %s";

    public static void main(String[] args)  throws Exception{
        SampleModel clickhouse = new SampleModel();
        clickhouse.setArr(new String[]{"aman","deep"});
        clickhouse.setX(0.5f);
        clickhouse.setId("hello");
        clickhouse.setValue("deep");


        Reflections reflections = new Reflections();
        Set<Class<?>> classesWithTableAnnotation = reflections.getTypesAnnotatedWith(ClickhouseTable.class,true);
        for(Class classwithAnnotation : classesWithTableAnnotation){
            ClickhouseTable annotation = (ClickhouseTable) classwithAnnotation.getAnnotation(ClickhouseTable.class);
            String orderingKeys[] = null;
            OrderingKeyColumns orderingKeyColumns = (OrderingKeyColumns)classwithAnnotation.getAnnotation(OrderingKeyColumns.class);
            if(orderingKeyColumns!=null){
                orderingKeys = orderingKeyColumns.orderingKeys();
            }
            String tableEngine  =annotation.tableEngine().getEngine();
            String tableName = annotation.tableName();

            Field[] fields = classwithAnnotation.getDeclaredFields();
            Map<String,KeyAndType> map = new HashMap<>();
            for(Field field : fields){
                field.setAccessible(true);
                if(!field.isAnnotationPresent(Transient.class)){
                    KeyAndType keyAndType = getKeyAndType(field);
                    map.put(field.getName(),keyAndType);
                }
            }
            StringBuilder builder = new StringBuilder();
            map.forEach((key,value)->{
                builder.append(getSQLRepresentationOfColumn(value));
                builder.append(",").append("\n");
            });
            builder.append(")");
            String createStatement =  String.format(CREATE_TABLE_PREFIX,tableName,builder.toString());
            System.out.printf(createStatement);
        }
    }

    private static String getSQLRepresentationOfColumn(KeyAndType keyAndType){
        return String.format(SQL_COLUMN_AND_TYPE,keyAndType.getKey(),keyAndType.getType());
    }

    private String getSQLForTableCreation(){
        return null;
    }

    private static  final Connection getConnection(){
        try{
        Class.forName("com.github.housepower.jdbc.ClickHouseDriver");
        Connection connection = DriverManager.getConnection("jdbc:clickhouse://127.0.0.1:9000");
        return  connection;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }


    private static KeyAndType getKeyAndType(Field field){
        ClickhouseColumn columnAnnotation = field.getAnnotation(ClickhouseColumn.class);
        KeyAndType instance = new KeyAndType();
        if(columnAnnotation!=null){
            String name = columnAnnotation.name();
            String type = columnAnnotation.type();
            instance.setKey(name);
            instance.setType(type);
        }else{
            instance.setKey(field.getName());
            setPreferredType(field,instance);
        }

        return instance;
    }

    private static void setPreferredType(Field field, KeyAndType instance){
        if(field.getType().isArray()){
            Class c = field.getType().getComponentType();
            instance.setType(String.format(ARRAY_TYPE,c.getName()));
        }
        if(field.getType().isEnum()){

        }
        if(field.getDeclaringClass().isAssignableFrom(String.class)){
            instance.setType("String");
        }
        else if (field.getDeclaringClass().isAssignableFrom(Integer.class)){
            instance.setType("Int32");
        }else if(field.getDeclaringClass().isAssignableFrom(Long.class)){
            instance.setType("Int64");
        }else if(field.getDeclaringClass().isAssignableFrom(Short.class)){
            instance.setType("Int16");
        } else if(field.getDeclaringClass().isAssignableFrom(Byte.class)){
            instance.setType("Int8");
        } else if(field.getDeclaringClass().isAssignableFrom(Boolean.class)){
            instance.setType("UInt8");
        }else if(field.getDeclaringClass().isAssignableFrom(Float.class)){
            instance.setType("Float32");
        }else if(field.getDeclaringClass().isAssignableFrom(Double.class)){
            instance.setType("Float64");
        }else if(field.getDeclaringClass().isAssignableFrom(Date.class)){
            instance.setType("Date");
        }else if(field.getDeclaringClass().isAssignableFrom(LocalDateTime.class)){
            instance.setType("DateTime");
        }else if(field.getDeclaringClass().isAssignableFrom(Timestamp.class)){
            instance.setType("DateTime");
        }
    }
}
