package org.aman.clickhouse.annotations;


public @interface PartitioningKey {
    String value();

    String function() default "";
}
