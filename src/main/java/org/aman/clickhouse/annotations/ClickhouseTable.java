package org.aman.clickhouse.annotations;

import org.aman.clickhouse.annotations.enums.TableEngine;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ClickhouseTable {
    String tableName();

    TableEngine tableEngine();

    int indexGranularity() default 8192;
}
