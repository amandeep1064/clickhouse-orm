package org.aman.clickhouse.annotations.enums;

public enum TableEngine {
    MERGE_TREE("MergeTree"),
    REPLACING_MERGE_TREE("ReplacingMergeTree"),
    SUMMING_MERGE_TREE("SummingMergeTree"),
    AGGREGATING_MERGE_TREE("AggregatingMergeTree"),
    COLLAPSING_MERGE_TREE("CollapsingMergeTree"),
    GRAPHITE_MERGE_TREE("GraphiteMergeTree");


    private String tableEngine;

    TableEngine(String tableEngine) {
        this.tableEngine = tableEngine;
    }

    public String getEngine() {
        return this.tableEngine;
    }
}
