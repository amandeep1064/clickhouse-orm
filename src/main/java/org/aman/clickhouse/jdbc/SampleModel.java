package org.aman.clickhouse.jdbc;


import org.aman.clickhouse.annotations.ClickhouseColumn;
import org.aman.clickhouse.annotations.ClickhouseTable;
import org.aman.clickhouse.annotations.OrderingKeyColumns;
import org.aman.clickhouse.annotations.enums.TableEngine;

@ClickhouseTable(tableName = "abc", tableEngine = TableEngine.MERGE_TREE)

@OrderingKeyColumns(orderingKeys = {"id", "val"})
public class SampleModel {

    @ClickhouseColumn(name = "id", type = "String")
    private String id;

    @ClickhouseColumn(name = "val", type = "String")
    private String value;

    @ClickhouseColumn(name = "list", type = "Array(String)")
    private String[] arr;

    @ClickhouseColumn(name = "x", type = "Float32")
    private Float x;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String[] getArr() {
        return arr;
    }

    public void setArr(String[] arr) {
        this.arr = arr;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }
}
